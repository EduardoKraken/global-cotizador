import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'
import store from '@/store'

import Login                        from '@/views/usuario/Login.vue'
import RecuperarPassword            from '@/views/usuario/RecuperarPassword.vue'

import Usuarios                     from '@/views/administracion/Usuarios.vue'
import Permisos                     from '@/views/administracion/Permisos.vue'
import Puestos                      from '@/views/administracion/Puestos.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: '',
  base: process.env.BASE_URL,
  routes: [

    { path: '/home'          , name: 'Home'           , component: Home, 
      meta: { ADMIN: true, USUARIO: true, NORMAL: true, libre: true }},
    
    { path: '/'              , name: 'Login'          , component: Login, 
      meta: { ADMIN: true, USUARIO: true, NORMAL: true, libre: true }},

    { path: '/recuperarpassword'      , name: 'RecuperarPassword'          , component: RecuperarPassword, 
      meta: { ADMIN: true, USUARIO: true, NORMAL: true, libre: true }},

    // Administración
    { path: '/usuarios'      , name: 'Usuarios'       , component: Usuarios, 
      meta: { ADMIN: true, USUARIO: true, NORMAL: true, libre: true }},
    { path: '/puestos'       , name: 'Puestos'        , component: Puestos, 
      meta: { ADMIN: true, USUARIO: true, NORMAL: true, libre: true }},
    { path: '/permisos'      , name: 'Permisos'       , component: Permisos, 
      meta: { ADMIN: true, USUARIO: true, NORMAL: true, libre: true }},
    
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.libre)) {
    next()
  } else if (store.state.login.datosUsuario) {
    if (to.matched.some(record => record.meta.ADMIN)) {
      next()
    }
  } else {
    next({
      name: 'Login'
    })
  }
})

export default router