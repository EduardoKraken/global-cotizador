import Vue from 'vue'
import { mapGetters,mapActions } from 'vuex';

var XLSX = require("xlsx");

export default {
	methods: {
    exportExcel(dataExport, name){
      let data = XLSX.utils.json_to_sheet(dataExport)
      const workbook = XLSX.utils.book_new()
      const filename = name
      XLSX.utils.book_append_sheet(workbook, data, filename)

      var wbout = XLSX.write(workbook, {
        bookType: 'xls',
        bookSST: false,
        type: 'binary'
      });

      const file = new File([new Blob([this.s2ab(wbout)])], filename + '.xls')
      
      let formData = new FormData();
      // //se crea el objeto y se le agrega como un apendice el archivo 
      formData.append('file',file);
      // /*getDatosRiesgo the form data*/

      this.$http.post('pdfs',formData).then(response=> {
        window.location = this.$http.options.root + 'pdfs/' + filename + '.xls'
      }).catch(error=> {
        console.log(error);
      });
    },

    s2ab(s) {
      var buf = new ArrayBuffer(s.length);
      var view = new Uint8Array(buf);
      for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    },

  }
}